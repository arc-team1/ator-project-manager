
import logging
from datetime import date, timedelta,datetime
from date_utility import normalization_year
from date_utility import get_weeknum_fromdate,get_date_fromweeknum
from date_utility import findDay_name,findMonth_name

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

"""
yy-mm-gg

dammi la lista dei progetti di lunedì scorso 2019-11-18
dammi la lista dei progetti di questo mese 2019-11
dammi la lista dei progetti questa settimana 2019-W47
dammi la lista dei progetti di novembre 2020-11
"""


def get_interval_month(now_date,searched_date):

	"""
	dammi la lista dei progetti
	di questo mese

	date example:2019-11
	"""

	complete_date = now_date.strftime("%Y-%m-%d")
	now_year, now_month, now_day = complete_date.split('-')

	date_str = (searched_date).split('-')
	year, month, day = [date_str[i] if i < len(date_str) else None for i in range(3)]

	start_current_month = date(int(year), int(month), 1)
	today = date(int(now_year), int(now_month), int(now_day))

	start_month_day = findDay_name(str(start_current_month))
	end_month_day = findDay_name(str(today))

	start_month_name = findMonth_name(int(month))
	end_month_name = findMonth_name(int(now_month))

	if now_year == year and now_month == month:
		logger.info("CURRENT MONTH")
		grain_month = {
			'grain': 'month',
			'current_month':True,
			'from': start_current_month,
			'to': today,
			'searched_data': searched_date,
			'start_month_day':start_month_day,
			'end_month_day': end_month_day,
			'start_month_name':start_month_name,
			'end_month_name':end_month_name
		}
		return grain_month
	else:
		logger.info("NOT CURRENT MONTH")

		# TODO passarsi i nomi dei giorni della settimana, del mese
		grain_month = {
			'grain': 'month',
			'current_month': False,
			'from': '',
			'to': '',
			'searched_data': searched_date,
			'start_month_day': '',
			'end_month_day': '',
			'start_month_name':''
		}
		return grain_month


def get_interval_year(now_date,searched_date):

	"""
	dammi i progetti
	di questo anno

	date example: 2019
	"""

	complete_date = now_date.strftime("%Y-%m-%d")
	now_year, now_month, now_day = complete_date.split('-')

	date_str = (searched_date).split('-')
	year, month, day = [date_str[i] if i < len(date_str) else None for i in range(3)]

	from_date = date(int(year), int(1), 1)  # da inizo mese
	to_date = date(int(now_year), int(now_month), int(now_day))  # ad oggi

	start_year_month_name = findMonth_name(1)
	end_year_month_name = findMonth_name(int(now_month))

	if now_year == year:
		logger.info("CURRENT YEAR")
		grain_year = {
			'grain': 'year',
			'current_year': True,
			'from': from_date,
			'to': to_date,
			'searched_data': searched_date,
			'start_year_month_name':start_year_month_name,
			'end_year_month_name':end_year_month_name
		}
		return grain_year
	else:
		logger.info("NOT CURRENT MONTH")
		#TODO fare not current year
		grain_year = {
			'grain': 'year',
			'current_year': False,
			'searched_data': searched_date,
			'start_year_month_name': '',
			'end_year_month_name': ''
		}
		return grain_year


def get_exact_day(now_date,searched_date):

	complete_date = now_date.strftime("%Y-%m-%d")
	now_year, now_month, now_day = complete_date.split('-')

	date_str = (searched_date).split('-')
	year, month, day = [date_str[i] if i < len(date_str) else None for i in range(3)]

	start_day_name = findDay_name(searched_date)

	month_name = findMonth_name(int(month))

	if now_year == year and now_month == month and now_day == day:
		# giorno esatto durante il mese corrente

		logger.info("CURRENT DAY IN CURRENT MONTH")

		grain_day = {
			'grain': 'day',
			'current_day': True,
			'from': searched_date,
			'to': searched_date,
			'searched_data': searched_date,
			'start_day_name': start_day_name,
			'month_name': month_name,
		}
		return grain_day
	else:
		# TODO da gestire not current day
		logger.info("NOT CURRENT DAY")
		grain_day = {
			'grain': 'day',
			'current_day': False,
			'from': searched_date,
			'to': searched_date,
			'searched_data': searched_date
		}
		return grain_day


def get_interval_week(now_date,searched_date):

	"""
	dammi la lista dei progetti
	di questa settimana

	date example: 2019-W47
	"""

	now_year, now_week_num, now_day_week = get_weeknum_fromdate(now_date)

	date_str = (searched_date).split('-')
	year, week_num, day = [date_str[i] if i < len(date_str) else None for i in range(3)]

	if now_year == year and now_week_num == week_num:
		logger.info("CURRENT WEEK")

		day = get_date_fromweeknum(searched_date)

		start_day_week_str = day.strftime("%Y-%m-%d")
		start_week_day_name = findDay_name(start_day_week_str)

		end_day_week_str = now_date.strftime("%Y-%m-%d")
		end_week_day_name = findDay_name(end_day_week_str)

		grain_week = {
			'grain': 'week',
			'current_week': True,
			'from': day,
			'to': now_date,
			'searched_data': searched_date,
			'start_week_day': start_week_day_name,
			'end_week_day': end_week_day_name,
			'week_num':week_num
		}
		return grain_week
	else:
		logger.info("NOT CURRENT WEEK")
		start_week_day = get_date_fromweeknum(searched_date)

		start_day_week_str = start_week_day.strftime("%Y-%m-%d")
		start_week_day_name = findDay_name(start_day_week_str)

		end_day_week_str = now_date.strftime("%Y-%m-%d")
		end_week_day_name = findDay_name(end_day_week_str)
		#TODO da rifare prché + 1 week
		#TODO passarsi il numero della settimana
		grain_week = {
			'grain': 'week',
			'current_week': False,
			'from': start_week_day,
			'to': now_date,
			'searched_data':now_date,
			'start_week_day': start_week_day_name,
			'end_week_day': end_week_day_name,
		}
		return grain_week


def get_interval_data(searched_date):

	now_date= date.today()
	complete_date = now_date.strftime("%Y-%m-%d")
	now_year, now_month, now_day = complete_date.split('-')

	date_str = (searched_date).split('-')
	year, month, day = [date_str[i] if i < len(date_str) else None for i in range(3)]
	year = normalization_year(now_year, year)

	if day is None and month is not None and year is not None and len(month) == 2:
		#2019-11
		grain_month = get_interval_month(now_date, searched_date)
		return grain_month

	elif day is None and month is None and year is not None:
		#2019
		grain_year = get_interval_year(now_date, searched_date)
		return grain_year

	elif now_year == year and now_month == month:
		# 2019-11-1
		grain_day = get_exact_day(now_date, searched_date)
		return grain_day

	elif day is None and year is not None and year == now_year and len(month) > 2:
		#2019-W47
		grain_week = get_interval_week(now_date, searched_date)
		return grain_week


from postgres_data import search_project
from dialogues_templates import speech_projects_list



def testing_day():

	# dammi la lista dei progetti di oggi
	print("Test 1: Current Day")
	print("dammi la lista dei progetti di oggi")
	t1 = search_project('2019-11-23')
	print(t1)


def testing_speechtext_week():

	print("TEST 5: current week")
	print("dammi la lista dei progetti di questa settimana")
	t5 = speech_projects_list('2019-W47')
	print(t5)

	print("TEST 6: not current week")
	t6 = speech_projects_list('2019-W46')
	print(t6)


def	testing_speechtext_month():

	print("TEST 3: current year and month")
	print("dammi la lista dei progetti di questo mese")
	t3 = speech_projects_list('2019-11')
	print(t3)

	# TODO to fix from
	# print("TEST 4: current year and last month")
	# t4 = speech_projects_list('2019-10')
	# print(t4)


def testing_speechtext_year():

	print("TEST 1: current year")
	print("dammi la lista dei progetti di questo anno")
	t1 = search_project('2019')
	print(t1)

	#TODO to fix from
	print("TEST 2: not current year")
	print("dammi la lista dei progetti dell'anno scorso")
	#t2 = speech_projects_list('2018')
	#print(t2)







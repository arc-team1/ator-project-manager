import calendar
from datetime import datetime



def extract_year(complete_date):
	complete_date = complete_date.strftime("%Y-%m-%d")
	year, mm, gg = complete_date.split('-')
	return year


def findMonth_name(monthNumber):

	months = ["gennaio", "febbraio", "marzo", "aprile","maggio","giugno",
			  "luglio","agosto", "settembre","ottobre","novembre","dicembre"]

	for i in range(len(months)):
		if i+1 == monthNumber:
			monthName = months[i]
			return monthName


def findDay_name(date):

	"""
	Restituisco il nodme del giorno
	della settimana
	"""

	year,month,dd = (int(i) for i in date.split('-'))
	dayNumber = calendar.weekday(year, month, dd)

	days = ["lunedì", "martedì", "mercoledì", "giovedì",
			"venerdì", "sabato", "domenica"]


	day = days[dayNumber]
	return day


def normalization_year(now_year, year):
	"""
	dammi la lista dei progetti
	di novembre
	2020-11
	"""

	if year > now_year:
		return now_year
	else:
		return now_year


def get_date_fromweeknum(date):

	"""
	The -1 and -%w pattern tells the parser
	to pick the Monday in that week

	%G, %V, %u are ISO equivalents
	of %Y, %W, %w

	"""

	day = datetime.strptime(date + '-1', '%G-W%V-%u')
	day = day.date()
	return day


def get_weeknum_fromdate(now_date):

	now_year, now_week_num, now_day_week = now_date.isocalendar()
	now_week_num = "W" + str(now_week_num)
	now_year = str(now_year)
	return now_year, now_week_num, now_day_week
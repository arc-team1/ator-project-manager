
import logging
import six

from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.utils import is_request_type, is_intent_name
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_model.ui import SimpleCard
from ask_sdk_model import Response
from ask_sdk_model import ui

from ask_sdk_model.interfaces.audioplayer import (PlayDirective, PlayBehavior, AudioItem, Stream, AudioItemMetadata,
    												StopDirective, ClearQueueDirective, ClearBehavior)

from dialogues_templates import project_info,speech_projects_list
from postgres_data import search_project_row


sb = SkillBuilder()

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)



@sb.request_handler(can_handle_func=is_request_type("LaunchRequest"))
def launch_request_handler(handler_input):

	atributes_dict = {
		"id_spredsheet": '',
		"projects_index": ''
	}

	handler_input.attributes_manager.session_attributes = atributes_dict

	#attribute = handler_input.attributes_manager.session_attributes

	speech_text = "Sono Arc Team Manager. Che cosa vuoi fare?"

	card_image = "https://maps.google.com/maps/api/staticmap?key=AIzaSyDra76kQywFKBaTomfyn4YB0YBk-ZxaBps&maptype=satellite&format=png&scale=1&center=45.874000,10.885972&zoom=17&size=400x400&sensor=false&language=en"
	card = ui.StandardCard(title="Welcome",text=speech_text,image=ui.Image(small_image_url=card_image,large_image_url=card_image))
	res = handler_input.response_builder.speak(speech_text).set_card(card).set_should_end_session(False).response
	return res


@sb.request_handler(can_handle_func=is_intent_name("newProjectIntent"))
def newproject_intent_handler(handler_input):

	speech_text = "Sto creando un progetto di default"

	res = handler_input.response_builder.speak(speech_text).set_card(
		SimpleCard("Create Project", speech_text)).set_should_end_session(
		True).response

	return res


@sb.request_handler(can_handle_func=is_intent_name("openProject"))
def openproject_intent_handler(handler_input):

	attribute = handler_input.attributes_manager.session_attributes

	slots = handler_input.request_envelope.request.intent.slots

	for slot_name, current_slot in six.iteritems(slots):
		project_name = slots[slot_name]
		entity_value = project_name.value

		if entity_value is None:
			logger.info("Project without entities")
			speech_text = ("devi dirmi il nome di un progetto o prova a chiedermi: dammi la lista dei progetti di questo mese")
			res = handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Open Project", speech_text)).set_should_end_session(False).response
			return res
		else:
			row_id = search_project_row(entity_value)
			if row_id is not None:
				speech_text, card_image = project_info(row_id)

				card_title = " Project {} - Immagine satellitare".format(entity_value)
				card = ui.StandardCard(title=card_title,text=speech_text,image=ui.Image(small_image_url=card_image,large_image_url=card_image))
				res = handler_input.response_builder.speak(speech_text).set_card(card).set_should_end_session(False).response
				return res
			else:
				speech_text = "mi dispiace non ho trovato il progetto che stavi cercando. Prova a chiedermi: dammi la lista dei progetti di questo mese"
				card_image = "https://maps.google.com/maps/api/staticmap?key=AIzaSyDra76kQywFKBaTomfyn4YB0YBk-ZxaBps&maptype=satellite&format=png&scale=1&center=45.874000,10.885972&zoom=17&size=400x400&sensor=false&language=en"
				card_title = " Project {} - Immagine satellitare".format(entity_value)
				card = ui.StandardCard(title=card_title, text=speech_text,image=ui.Image(small_image_url=card_image, large_image_url=card_image))
				res = handler_input.response_builder.speak(speech_text).set_card(card).set_should_end_session(False).response
				return res


@sb.request_handler(can_handle_func=is_intent_name("takeNote"))
def takenote_intent_handler(handler_input):

	attribute = handler_input.attributes_manager.session_attributes

	slots = handler_input.request_envelope.request.intent.slots

	for slot_name, current_slot in six.iteritems(slots):
		project_name = slots[slot_name]
		entity_value = project_name.value

		if entity_value is None:
			logger.info("Note without description")
			speech_text = ("Prova con predi note che ho iniziato lo scavo presso.. ")
			res = handler_input.response_builder.speak(speech_text).set_card(
						SimpleCard("Diario scavo archeologico", speech_text)).set_should_end_session(False).response

			return res
		else:
			logger.info("Take a note")
			speech_text = entity_value
			res = handler_input.response_builder.speak(speech_text).set_card(
				SimpleCard("Diario scavo archeologico", speech_text)).set_should_end_session(False).response

			return res



@sb.request_handler(can_handle_func=is_intent_name("listProjects"))
def projectList_intent(handler_input):

	slots = handler_input.request_envelope.request.intent.slots
	for slot_name, current_slot in six.iteritems(slots):
		project_name = slots[slot_name]
		entity_value = project_name.value

		if entity_value is None:
			logger.info("Request without date")
			speech_text = ("devi darmi almeno una data per cercare, prova chiedendomi: dammi la lista dei progetti di questo mese")
			res = handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Lista dei progetti per data", speech_text)).set_should_end_session(False).response
			return res
		else:
			log = ("VALUE: {}").format(entity_value)
			logger.info(log)

			speech_text = speech_projects_list(entity_value)
			res = handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Lista dei progetti per data", speech_text)).set_should_end_session(False).response

			return res


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.HelpIntent"))
def help_intent_handler(handler_input):

	speech_text = "prova a chiedermi di aprire un progetto, chiedi la lista dei progetti"

	res = handler_input.response_builder.speak(speech_text).ask(speech_text).set_card(SimpleCard("Help", speech_text)).response
	return res


@sb.request_handler(
	can_handle_func=lambda handler_input:
	is_intent_name("AMAZON.CancelIntent")(handler_input) or
	is_intent_name("AMAZON.StopIntent")(handler_input))
def cancel_and_stop_intent_handler(handler_input):
	speech_text = "Arrivederci!"
	res = handler_input.response_builder.speak(speech_text).set_card(SimpleCard("Hello World", speech_text)).response
	return res



@sb.request_handler(can_handle_func=is_request_type("SessionEndedRequest"))
def session_ended_request_handler(handler_input):
	return handler_input.response_builder.response


@sb.exception_handler(can_handle_func=lambda i, e: True)
def all_exception_handler(handler_input, exception):

	logger.error(exception, exc_info=True)
	speech = "C'è stato un problema."
	handler_input.response_builder.speak(speech).ask(speech)

	res = handler_input.response_builder.response
	return res


handler = sb.lambda_handler()

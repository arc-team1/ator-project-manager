
from date_utility import findDay_name,findMonth_name


def pagination_projects_week(projects):

	speechtext_list = []
	for project in projects:
		project_name = project[0]
		project_city = project[1]
		project_year = project[2]

		project_year = project_year.strftime("%Y-%m-%d")
		day_name = findDay_name(project_year)
		year,month, day = project_year.split('-')

		speechtext_project = " {} {} a {} il '{}',".format(day_name,day,project_city,project_name)
		speechtext_list.append(speechtext_project)

	speech= (' ').join(speechtext_list)
	return speech


def pagination_projects_month(projects):

	speechtext_list = []
	for project in projects:
		project_name = project[0]
		project_city = project[1]
		project_year = project[2]

		project_year = project_year.strftime("%Y-%m-%d")
		day_name = findDay_name(project_year)
		year,month, day = project_year.split('-')

		speechtext_project = " {} {} a {} il '{}',".format(day_name,day,project_city,project_name)
		speechtext_list.append(speechtext_project)

	speech= (' ').join(speechtext_list)
	return speech


def pagination_projects_day(projects):

	speechtext_list = []
	for project in projects:
		project_name = project[0]
		project_city = project[1]
		project_year = project[2]

		project_year = project_year.strftime("%Y-%m-%d")
		day_name = findDay_name(project_year)
		year,month, day = project_year.split('-')

		speechtext_project = " {} {} a {} il '{}',".format(day_name,day,project_city,project_name)
		speechtext_list.append(speechtext_project)

	speech= (' ').join(speechtext_list)
	return speech


def pagination_projects_year(projects):

	"""
	giovedì 21 novembre a padova il bambino di taung
	"""

	speechtext_list = []
	for project in projects:
		project_name = project[0]
		project_city = project[1]
		project_year = project[2]

		project_year = project_year.strftime("%Y-%m-%d")
		day_name = findDay_name(project_year)
		year,month, day = project_year.split('-')

		month_name = findMonth_name(int(month))

		speechtext_project = " {} {} {} a {} il '{}',".format(day_name,day,month_name,project_city,project_name)
		speechtext_list.append(speechtext_project)

	speech= (' ').join(speechtext_list)
	return speech
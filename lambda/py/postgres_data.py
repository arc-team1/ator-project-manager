import psycopg2
from datetime import date
from date_format import get_interval_data
from date_utility import extract_year



con = psycopg2.connect(
	host = "0.tcp.ngrok.io",
	database = "db_arcteam",
	user = "postgres",
	password = "",
	port = ""
)


def search_project_row(searched_project_name):

	"""
	In base al nome del progetto
	cercato dall'utente
	restituisco l'id

	"""

	cur = con.cursor()
	query = "SELECT id,progetto,anno FROM progetti WHERE progetto = '{}';".format(searched_project_name)
	cur.execute(query)
	rows = cur.fetchall()

	today = date.today()
	for row in rows:
		row = list(row)
		row_id = row[0]
		project_name = row[1]
		project_year = row[2]

		project_year = extract_year(project_year)
		current_year = extract_year(today)

		spn = searched_project_name.lower()
		pn = project_name.lower()

		if (current_year == project_year) and (spn == pn):
			return row_id


def get_project_info(row_id):

	cur = con.cursor()
	db_query = "SELECT progetto, comune, anno, lat, lon FROM progetti WHERE  id ={};".format(row_id)
	cur.execute(db_query)

	rows = cur.fetchall()
	for row in rows:
		row = list(row)
		return row


def search_project_dat(from_day,to_day):

	cur = con.cursor()
	query = "SELECT id,progetto, comune,anno FROM progetti WHERE '[{}, {}]'::daterange @> anno".format(from_day, to_day)
	cur.execute(query)
	rows = cur.fetchall()

	project_list = []
	for row in rows:
		row = list(row)
		projectFounded = get_project_info(row[0])
		project_list.append(projectFounded)

	return project_list


def search_project(searched_project_data):

	grain = get_interval_data(searched_project_data)
	from_day = grain['from']
	to_day = grain['to']

	if grain['grain'] == 'month':
		founded_projects = search_project_dat(from_day,to_day)
		return grain,founded_projects

	elif grain['grain'] == 'year':
		founded_projects = search_project_dat(from_day, to_day)
		return grain,founded_projects

	elif grain['grain'] == 'day':
		founded_projects = search_project_dat(from_day, to_day)
		return grain,founded_projects

	# dammi la lista dei progetti di questa settimana
	elif grain['grain'] == 'week':
		founded_projects = search_project_dat(from_day, to_day)
		return grain,founded_projects


def test():
	cur = con.cursor()
	db_query = "INSERT INTO scavi (id,descrizione,anno,progetto) values(3,'inizio scavi a castello blabla blaaala','2019-11-22','bambino di taung');"
	cur.execute(db_query)
	con.commit()
	con.close()


test()
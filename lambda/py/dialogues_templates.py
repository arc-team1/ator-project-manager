from postgres_data import search_project,get_project_info
from project_maps import satelliteImage
from dialogues_pagination import pagination_projects_month,pagination_projects_week,pagination_projects_year,pagination_projects_day


def project_info(row_id):

	project_info = get_project_info(row_id)
	project_name = project_info[1]
	project_city = project_info[1]
	project_year = project_info[2]
	project_year = project_year.strftime("%Y-%m-%d")

	speech_text = "ho trovato il progetto archeologico {} a {} del {}. " \
				  "Adesso puoi modificare il progetto, " \
				  "le unità stratigrafiche, i fotopiani".format(project_name, project_city, project_year)

	# create image from satellite
	project_lat = project_info[3]
	project_lon = project_info[4]
	urlimage = satelliteImage(project_lat, project_lon)

	return speech_text, urlimage


def get_speechtext_month(grain,num_project):

	START_MOMTH_DAY_NAME = grain['start_month_day']
	END_MONTH_DAY_NAME = grain['end_month_day']
	START_MONTH_NAME = grain['start_month_name']
	END_MONTH_NAME = grain['end_month_name']


	if grain['current_month'] is True:
		if num_project == 0:
			speech_text = "Da inizio {} ad oggi {} mese non hai lavorato".format(START_MONTH_NAME, END_MONTH_DAY_NAME)
			return speech_text
		elif num_project == 1:
			speech_text = "Nel mese di {} hai lavorato ad un solo progetto".format(START_MONTH_NAME,num_project)
			return speech_text
		elif num_project > 1:
			speech_text = "Durante il mese di {} hai lavorato a {} progetti:".format(START_MONTH_NAME,num_project)
			return speech_text
	else:
		speech_text = "Attento!Stai cercando un progetto che non è di questo mese"
		return speech_text


def get_speechtext_year(grain,num_project):

	NUM_PROJECT = num_project
	START_YEAR_MONTH_NAME = grain['start_year_month_name']
	END_YEAR_MONTH_NAME = grain['end_year_month_name']

	if grain['current_year'] is True:
		if num_project == 0:
			speech_text = "Da {} a {} non hai lavorato".format(START_YEAR_MONTH_NAME,END_YEAR_MONTH_NAME)
			return speech_text
		elif num_project == 1:
			speech_text = "Da {} a {} hai lavorato ad un solo progetto".format(START_YEAR_MONTH_NAME,END_YEAR_MONTH_NAME)
			return speech_text
		elif num_project > 1:
			speech_text = "Da {} a {} hai lavorato a {} progetti:".format(START_YEAR_MONTH_NAME,END_YEAR_MONTH_NAME, NUM_PROJECT)
			return speech_text
	else:
		speech_text = "Attento!Stai cercando un progetto che non è di questo anno"
		return speech_text


def get_speechtext_day(grain,num_project):

	if grain['current_day'] is True:
		if num_project == 0:
			speech_text = "Oggi non hai lavorato"
			return speech_text
		elif num_project == 1:
			speech_text = "Oggi hai lavorato ad un solo progetto".format(num_project)
			return speech_text
		elif num_project > 1:
			speech_text = "Oggi hai lavorato a {} progetti:".format(num_project)
			return speech_text
	else:
		speech_text = "Attento!Stai cercando un progetto di altri giorni"
		return speech_text


def get_speechtext_week(grain,num_project):

	start_week_day = grain['start_week_day']
	end_week_day = grain['end_week_day']

	if grain['current_week'] is True:
		if num_project == 0:
			speech_text = "Da {} a {} di questa settimana non hai lavorato".format(start_week_day,end_week_day)
			return speech_text
		elif num_project == 1:
			speech_text = "Da {} a {} di questa settimana hai lavorato ad un solo progetto".format(start_week_day,end_week_day)
			return speech_text
		elif num_project > 1:
			speech_text = "Da {} a {} di questa settimana hai lavorato a {} progetti:".format(start_week_day,end_week_day,num_project)
			return speech_text
	else:
		speech_text = "Stai cercando un progetto di altre settimane"
		return speech_text


def speech_projects_list(searched_data):

	grain,projects = search_project(searched_data)
	num_project = len(projects)

	if grain['grain'] == 'month':
		speechtext_month = get_speechtext_month(grain,num_project)
		speechtext_pagination_month = pagination_projects_month(projects)
		speechtext = speechtext_month + speechtext_pagination_month
		return speechtext

	elif grain['grain'] == 'year':
		speechtext_year = get_speechtext_year(grain,num_project)
		speechtext_pagination = pagination_projects_year(projects)
		speechtext = speechtext_year + speechtext_pagination
		return speechtext

	elif grain['grain'] == 'day':
		speechtext_day = get_speechtext_day(grain,num_project)
		speechtext_pagination = pagination_projects_day(projects)
		speechtext = speechtext_day + speechtext_pagination
		return speechtext

	elif grain['grain'] == 'week':
		speechtext_week = get_speechtext_week(grain, num_project)
		speechtext_pagination = pagination_projects_week(projects)
		speechtext = speechtext_week + speechtext_pagination
		return speechtext
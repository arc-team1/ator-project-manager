BEGIN;
CREATE TABLE project(
  id serial PRIMARY KEY,
  nome CHARACTER VARYING NOT NULL UNIQUE,
  inizio DATE NOT NULL CHECK (EXTRACT(YEAR FROM inizio) >= 2005 AND EXTRACT(YEAR FROM inizio) <= EXTRACT(YEAR FROM CURRENT_DATE)),
  descrizione CHARACTER VARYING NOT NULL
);

INSERT INTO project(nome, inizio, descrizione) VALUES
  ('Castel Penede', '2019-11-22', 'Scavo archeologico del sito di castel Penede'),
  ('Bambino di Taung', '2018-09-24', 'Ricostruzione fisiognomica digitale dei resti scheletrici del fossile di Austrolopithecus Africanus denominato "Bambino di Taung"');
COMMIT;
